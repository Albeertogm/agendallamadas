package com.example.notes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;

public class AgregarNota extends AppCompatActivity {

    ImageView regresar;
    EditText editNombre, editNumero;
    ArrayList<String> nombre, numero;
    Button Guardar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_nota);

        nombre = new ArrayList<String>();
        numero = new ArrayList<String>();

        editNombre = (EditText) findViewById(R.id.editTextNombre);
        editNumero = (EditText)  findViewById(R.id.editTextPhone);
        Guardar = (Button) findViewById(R.id.btnAgregar);

        regresar = (ImageView) findViewById(R.id.ImageViewBack);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guardarDatos();
                enviarDatos();
            }
        });


    }

    public void guardarDatos(){
        String name;
        String numer;
        name=editNombre.getText().toString();
        numer=editNumero.getText().toString();
        nombre.add(name);
        numero.add(numer);
        Toast.makeText(getApplicationContext(), "Contacto guardado correctamente", Toast.LENGTH_LONG).show();

    }
    public void enviarDatos(){
        Intent intent = new Intent(this, verNota.class);
        intent.putExtra("NotaArray", nombre);
        startActivity(intent);
    }

}
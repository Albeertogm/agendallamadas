package com.example.notes;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;

public class verNota extends AppCompatActivity {

    ListView ListaNotas;
    ImageView regresar;
    ArrayAdapter<String> adapter;
    ArrayList<String> nombreTarea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_nota);
        nombreTarea = new ArrayList<String>();

        if (getIntent().getSerializableExtra("NotaArray") == null){
            nombreTarea.add("Sin notas agregadas");
        }else {
            nombreTarea = (ArrayList<String>)getIntent().getSerializableExtra("NotaArray");
        }
        ListaNotas = (ListView) findViewById(R.id.listContactos);
        adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nombreTarea);
        // -- listview donde se mostraran los contactos
        ListView lista = (ListView) findViewById(R.id.listContactos);
        // -- adapter, sirve para mostrar algo en la list view en este caso se le pasa el arreglo nom (nombres)

        lista.setAdapter(adapter); // -- se hace un set del adapter en la listview

        // -- método al seleccionar un item de la lista
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // -- obtenemos la posicion en el array del nombre seleccionado en la ListView

                // -- intent para pasar los arreglos y la posición al siguiente activity
                Intent i = new Intent(verNota.this, AgregarNota.class);
                // -- arreglos
                //i.putStringArrayListExtra( "arrayNotas",  );

                startActivity(i);
            }

        });


        ListaNotas.setAdapter(adapter);
        
        regresar = (ImageView) findViewById(R.id.ImageViewBack);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}